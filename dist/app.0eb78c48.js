// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"assets/scripts/count.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.brute = exports.step = exports.big = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var SUBJECT_NUMBER = 3;
var TIMEOUT = 5000; // TODO: move constants to its own module.
// TODO: Clean up this stuff into a class?
// b looks at tenth power and above
// and finds the number within
// Using this pattern
// 10  - 1
// 100 - 20
// 1000 - 300
// and so on...
// d * (10^(p-1) * p)
// m looks at the case when the first
// digit matches the digit we want to count.
// 
// e are the digits to the right as a number
// b(d, p) + e(d, n, p) + 1
// above
// 59 -> b

function below(digit, power) {
  digit = parseInt(digit);
  return power === 1 ? digit : digit * (Math.pow(10, power - 1) * power);
}

function middle(digit, digits, power) {
  digits = _toConsumableArray(digits.toString());
  var index = digits.length - power;
  var excess = parseInt(digits.slice(index).join(''));
  return below(digit, power) + excess + 1;
}

function above(digit, power) {
  return below(digit, power) + Math.pow(10, power);
} // Use lookup table.


var big = function big(max) {
  var startTime = Date.now();
  if (max === 0) return {
    count: 0,
    numbers: 'n/a',
    duration: 0
  };

  var digits = _toConsumableArray(max.toString());

  var count = 0;
  var power = digits.length - 1;
  digits.forEach(function (digit) {
    digit = parseInt(digit);

    if (power > 0) {
      if (digit < SUBJECT_NUMBER) {
        count += below(digit, power);
      } else if (digit === SUBJECT_NUMBER) {
        count += middle(digit, max, power);
      } else if (digit > SUBJECT_NUMBER) {
        count += above(digit, power);
      }
    } else {
      count += digit >= SUBJECT_NUMBER ? 1 : 0;
    }

    power--;
  });
  return {
    count: count,
    numbers: 'There are too many to display.',
    duration: Date.now() - startTime
  };
}; // Generate


exports.big = big;

var step = function step(min, max) {
  return new Promise(function (resolve, reject) {
    var timeout = setTimeout(function () {
      reject(new Error('Exceeded timeout'));
    }, TIMEOUT);
    var startTime = Date.now();
    var count = 0;
    var numbers = [];
    var step = min;

    while (step <= max) {
      var digits = step.toString().split('');
      var matches = digits.filter(function (digit) {
        return parseInt(digit) === SUBJECT_NUMBER;
      }).length;

      if (matches > 0) {
        numbers.push(step);
        count += matches;
        step++;
      } else {
        // Jump!
        if (digits[digits.length - 1] == 0) {
          // If zero jump to num.
          step += SUBJECT_NUMBER;
        } else if (digits[digits.length - 1] < SUBJECT_NUMBER) {
          // If below jump to num.
          step += SUBJECT_NUMBER - digits[digits.length - 1];
        } else {
          // If above jump to tenth.
          step += Math.abs(10 - digits[digits.length - 1]);
        }
      }
    }

    clearTimeout(timeout);
    resolve({
      count: count,
      numbers: numbers,
      duration: Date.now() - startTime
    });
  });
}; // Brute force method to verify the result.
// noprotect


exports.step = step;

var brute = function brute(min, max) {
  return new Promise(function (resolve, reject) {
    var timeout = setTimeout(function () {
      reject(new Error('Exceeded timeout'));
    }, TIMEOUT);
    var startTime = Date.now();
    var count = 0;
    var numbers = [];

    for (var _step = min; _step <= max; _step++) {
      var digits = _step.toString().split('');

      var matches = digits.filter(function (digit) {
        return parseInt(digit) === SUBJECT_NUMBER;
      }).length;
      count += matches;
      numbers.push(_step);
    }

    clearTimeout(timeout);
    resolve({
      count: count,
      numbers: numbers,
      duration: Date.now() - startTime
    });
  });
};

exports.brute = brute;
},{}],"assets/scripts/elements.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ERRORS = exports.RESULT_NUMBERS = exports.RESULT_COUNT = exports.SUBMIT = exports.INPUT_MAX = exports.INPUT_MIN = exports.FORM = void 0;
var FORM = document.getElementById('form');
exports.FORM = FORM;
var INPUT_MIN = document.getElementById('input-min');
exports.INPUT_MIN = INPUT_MIN;
var INPUT_MAX = document.getElementById('input-max');
exports.INPUT_MAX = INPUT_MAX;
var SUBMIT = document.getElementById('submit');
exports.SUBMIT = SUBMIT;
var RESULT_COUNT = document.getElementById('result-count');
exports.RESULT_COUNT = RESULT_COUNT;
var RESULT_NUMBERS = document.getElementById('result-numbers');
exports.RESULT_NUMBERS = RESULT_NUMBERS;
var ERRORS = document.getElementById('errors');
exports.ERRORS = ERRORS;
},{}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/bundle-url.js":[function(require,module,exports) {
var bundleURL = null;

function getBundleURLCached() {
  if (!bundleURL) {
    bundleURL = getBundleURL();
  }

  return bundleURL;
}

function getBundleURL() {
  // Attempt to find the URL of the current script and use that as the base URL
  try {
    throw new Error();
  } catch (err) {
    var matches = ('' + err.stack).match(/(https?|file|ftp):\/\/[^)\n]+/g);

    if (matches) {
      return getBaseURL(matches[0]);
    }
  }

  return '/';
}

function getBaseURL(url) {
  return ('' + url).replace(/^((?:https?|file|ftp):\/\/.+)\/[^/]+$/, '$1') + '/';
}

exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
},{}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/css-loader.js":[function(require,module,exports) {
var bundle = require('./bundle-url');

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    link.remove();
  };

  newLink.href = link.href.split('?')[0] + '?' + Date.now();
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      if (bundle.getBaseURL(links[i].href) === bundle.getBundleURL()) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

module.exports = reloadCSS;
},{"./bundle-url":"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/bundle-url.js"}],"assets/style/app.scss":[function(require,module,exports) {
var reloadCSS = require('_css_loader');

module.hot.dispose(reloadCSS);
module.hot.accept(reloadCSS);
},{"_css_loader":"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/css-loader.js"}],"assets/scripts/test.js":[function(require,module,exports) {
"use strict";

var _count = require("./count");

// Simple test.
var testNumbers = [0, 1, 3, 5, 10, 12, 15, 13, 30, 33, 35, 120, 145, 124, 136, 355, 444, 333, 3411];
var index = 0;

var testAll = function testAll() {
  Promise.all([(0, _count.brute)(0, testNumbers[index]), (0, _count.step)(0, testNumbers[index])]).then(function (results) {
    var a = results[0].count;
    var b = results[1].count;
    var c = (0, _count.big)(testNumbers[index]).count;
    console.log(a === b && b === c ? true : false);
    index++;

    if (index < testNumbers.length) {
      testAll();
    }
  });
}; // testAll()
},{"./count":"assets/scripts/count.js"}],"assets/scripts/app.js":[function(require,module,exports) {
"use strict";

var _count = require("./count");

var ELEMENTS = _interopRequireWildcard(require("./elements"));

require("../style/app.scss");

require("./test");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var THRESHOLD = 20000;

function displayResult(result) {
  ELEMENTS.RESULT_COUNT.innerHTML = "There are <strong>".concat(result.count, "</strong> 3s.");

  if (Array.isArray(result.numbers)) {
    ELEMENTS.RESULT_NUMBERS.textContent = result.numbers.join(', ');
  } else {
    ELEMENTS.RESULT_NUMBERS.textContent = result.numbers;
  }
}

function clearResult() {
  ELEMENTS.RESULT_COUNT.textContent = '';
  ELEMENTS.RESULT_NUMBERS.textContent = '';
}

function displayErrors(errors) {
  ELEMENTS.ERRORS.style.display = 'block';
  ELEMENTS.ERRORS.innerHTML = "<ul>".concat(errors.map(function (error) {
    return "<li>".concat(error, "</li>");
  }).join(''), "</ul>");
}

function clearErrors() {
  ELEMENTS.ERRORS.style.display = 'none';
  ELEMENTS.ERRORS.innerHTML = '';
}

function filterInput(input) {
  // Remove everything that is not a number.
  return parseInt(input.replace(/[^\d]/g, ''));
}

var submit = function submit() {
  return new Promise(function (resolve, reject) {
    // Get filtered values.
    var min = filterInput(ELEMENTS.INPUT_MIN.value);
    var max = filterInput(ELEMENTS.INPUT_MAX.value); // Validate input.

    var errors = [];

    if (typeof min !== 'number' || isNaN(min)) {
      errors.push('Invalid min value, please enter a number.');
    }

    if (typeof min !== 'number' || isNaN(max)) {
      errors.push('Invalid max value, please enter a number.');
    }

    if (min > max) {
      errors.push('Min cannot be bigger than max.');
    } // Reject if input is not valid.


    if (errors.length > 0) {
      reject(errors);
    } // Use different algorithm depending on the scale of numbers.


    if (max - min > THRESHOLD) {
      var result = (0, _count.big)(max);
      result.count -= (0, _count.big)(min).count;
      resolve(result);
    } else {
      (0, _count.step)(min, max).then(function (result) {
        resolve(result);
      });
    }
  });
};

var formHandler = function formHandler(event) {
  event.preventDefault(); // Disable submit while form is being processed.

  ELEMENTS.SUBMIT.disabled = true;
  submit().then(function (result) {
    ELEMENTS.SUBMIT.disabled = false;
    clearErrors();
    displayResult(result);
  }).catch(function (errors) {
    ELEMENTS.SUBMIT.disabled = false;
    clearResult();
    displayErrors(errors);
  });
};

ELEMENTS.FORM.addEventListener('submit', formHandler);
},{"./count":"assets/scripts/count.js","./elements":"assets/scripts/elements.js","../style/app.scss":"assets/style/app.scss","./test":"assets/scripts/test.js"}],"../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61296" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","assets/scripts/app.js"], null)
//# sourceMappingURL=/app.0eb78c48.map