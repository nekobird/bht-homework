// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"node_modules/count/count.js":[function(require,module,exports) {
var global = arguments[3];
var define;
//! count.js
//! version : 0.1.4
//! authors : Nick Kaye
//! license : MIT
//! www.nickkaye.com/count-js

(function (undefined) {
  var count,
    VERSION = '0.1.4',
    words = {
      0: 'zero',
      1: 'one',
      2: 'two',
      3: 'three',
      4: 'four',
      5: 'five',
      6: 'six',
      7: 'seven',
      8: 'eight',
      9: 'nine',
      10: 'ten',
      11: 'eleven',
      12: 'twelve',
      13: 'thirteen',
      14: 'fourteen',
      15: 'fifteen',
      16: 'sixteen',
      17: 'seventeen',
      18: 'eighteen',
      19: 'nineteen',
      20: 'twenty',
      30: 'thirty',
      40: 'forty',
      50: 'fifty',
      60: 'sixty',
      70: 'seventy',
      80: 'eighty',
      90: 'ninety'
    },
    zeros = [
      {
        min: 2,
        max: 2,
        name: 'hundred'
      },
      {
        min: 3,
        max: 5,
        name: 'thousand'
      },
      {
        min: 6,
        max: 8,
        name: 'million'
      },
      {
        min: 9,
        max: 11,
        name: 'billion'
      },
      {
        min: 12,
        max: 14,
        name: 'trillion'
      }
    ],
    i,
    oldGlobalCount,
    hasOwnProperty = Object.prototype.hasOwnProperty,
    globalScope = typeof global !== 'undefined' ? global : this,
    hasModule = (typeof module !== 'undefined' && module.exports);

  /**
   * @param {Number} num
   * @returns {Array} words representing the number
   */
  function wordsForNumber(num) {
    var _n = num // number to work with
      , _p // power of ten
      , _m // minimum (calculator)
      , _i // iterator
      , _out = []; // output

    if (_n === 0) {
      _out.push(words[0]);
    } else {
      while (_n > 0) {
        //
        // from 1 to 19 e.g. "seventeen" (done)
        if (_n < 20) {
          _out.push(words[_n]);
          _n = 0;
        }
        // from 20 to 99 e.g. "eighty" (leave <10)
        else if (_n < 100) {
          _p = Math.floor(_n / 10);
          _out.push(words[_p * 10]);
          _n -= _p * 10;
        }
        // iterate through the possible x10 digits and reference zeros{} language dynamically
        else {
          for (_i in zeros) {
            if (_n < Math.pow(10, zeros[_i].max + 1)) {
              _m = Math.pow(10, zeros[_i].min);
              _p = Math.floor(_n / _m);
              _out = _out.concat(wordsForNumber(_p));
              _out.push(zeros[_i].name);
              _n -= _p * _m;
              break;
            }
          }
        }
      }
    }
    return _out;
  }

  function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  function defaultParsingFlags() {
    // We need to deep clone this object,
    // and es5 standard is not very helpful.
    return {
      empty: false,
      nullInput: false,
      userInvalidated: false
    };
  }

  function printMsg(msg) {
    if (count.suppressDeprecationWarnings === false &&
      typeof console !== 'undefined' && console.warn) {
      console.warn('Deprecation warning: ' + msg);
    }
  }

  function deprecate(msg, fn) {
    var firstTime = true;
    return extend(function () {
      if (firstTime) {
        printMsg(msg);
        firstTime = false;
      }
      return fn.apply(this, arguments);
    }, fn);
  }

  /**
   * Constructor
   * @constructs Count
   * @param config
   */
  function Count(config) {
    copyConfig(this, config);
  }

  /**
   * Copy configuration to target object
   * @param to
   * @param from
   * @returns {*}
   */
  function copyConfig(to, from) {
    if (typeof from._isACountObject !== 'undefined') {
      to._isACountObject = from._isACountObject;
    }
    if (typeof from._i !== 'undefined') {
      to._i = from._i;
    }
    if (typeof from._pf !== 'undefined') {
      to._pf = from._pf;
    }
    return to;
  }

  /**
   * Simple object extension
   * @param a
   * @param b
   * @returns {*}
   */
  function extend(a, b) {
    for (var i in b) {
      if (hasOwnProp(b, i)) {
        a[i] = b[i];
      }
    }
    if (hasOwnProp(b, 'toString')) {
      a.toString = b.toString;
    }
    if (hasOwnProp(b, 'valueOf')) {
      a.valueOf = b.valueOf;
    }
    return a;
  }

  /**
   * Top-level: Make a Count object
   * @param config
   * @returns {*}
   */
  function makeCount(config) {
    var input = config._i;
    if (config._i === null) {
      return count.invalid({nullInput: true});
    }
    if (typeof input !== 'number') {
      config._i = Number(input);
    }
    return new Count(config);
  }

  /**
   * Top-level: Make this accessible
   * @param input
   * @returns {*}
   */
  count = function (input) {
    var c;

    // object construction must be done this way.
    // https://github.com/nickckaye/count/issues/1423
    c = {};
    c._isACountObject = true;
    c._i = input;
    c._pf = defaultParsingFlags();

    return makeCount(c);
  };

  count.suppressDeprecationWarnings = false;

  count.createFromInputFallback = deprecate(
      'count construction falls back to js Date. This is ' +
      'discouraged and will be removed in upcoming major ' +
      'release. Please refer to ' +
      'https://github.com/nickckaye/count/issues/1407 for more info.',
    function (config) {
      config._d = new Date(config._i);
    }
  );

  // version number
  count.version = VERSION;

  // compare count object
  count.isCount = function (obj) {
    return obj instanceof Count ||
      (obj != null && hasOwnProp(obj, '_isACountObject'));
  };

  count.invalid = function (flags) {
    var m = count(0);
    if (flags != null) {
      extend(m._pf, flags);
    } else {
      m._pf.userInvalidated = true;
    }
    return m;
  };

  /**
   * Count Prototype
   */

  extend(count.fn = Count.prototype, {

    /**
     * Create a camel case block out of the input.
     * @returns {string}
     */
    camel: function () {
      var out = '',
        words = this.words();
      for (i = 0; i < words.length; i++) {
        out += capitalize(words[i]);
      }
      return out;
    },

    /**
     * Get the sequence of words that spells out the count
     */
    words: function () {
      return wordsForNumber(this._i);
    },

    clone: function () {
      return count(this);
    },

    valueOf: function () {
      return +this._i;
    },

    parsingFlags: function () {
      return extend({}, this._pf);
    }

  });

  /**
   * Exposing Count
   */

  function hasOwnProp(a, b) {
    return hasOwnProperty.call(a, b);
  }

  function makeGlobal(shouldDeprecate) {
    /*global ender:false */
    if (typeof ender !== 'undefined') {
      return;
    }
    oldGlobalCount = globalScope.count;
    if (shouldDeprecate) {
      globalScope.count = deprecate(
          'Accessing Count through the global scope is ' +
          'deprecated, and will be removed in an upcoming ' +
          'release.',
        count);
    } else {
      globalScope.count = count;
    }
  }

  // CommonJS module is defined
  if (hasModule) {
    module.exports = count;
  } else if (typeof define === 'function' && define.amd) {
    define('count', function (require, exports, module) {
      if (module.config && module.config() && module.config().noGlobal === true) {
        // release the global variable
        globalScope.count = oldGlobalCount;
      }

      return count;
    });
    makeGlobal(true);
  } else {
    makeGlobal();
  }
}).call(this);

},{}],"app.js":[function(require,module,exports) {
"use strict";

var _count = _interopRequireDefault(require("count"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Main
alert((0, _count.default)(1, 10));
},{"count":"node_modules/count/count.js"}],"../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "58139" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","app.js"], null)
//# sourceMappingURL=/app.c328ef1a.map