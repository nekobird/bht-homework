import {
  brute,
  step,
  big
} from './count';

// Simple test.
let testNumbers = [0, 1, 3, 5, 10, 12, 15, 13, 30, 33, 35, 120, 145, 124, 136, 355, 444, 333, 3411];

let index = 0;
const testAll = () => {
  Promise
    .all([brute(0, testNumbers[index]), step(0, testNumbers[index])])
    .then(results => {
      let a = results[0].count;
      let b = results[1].count;
      let c = big(testNumbers[index]).count;
      console.log(a === b && b === c ? true : false);
      index++;
      if (index < testNumbers.length) {
        testAll();
      }
    })
}

// testAll()