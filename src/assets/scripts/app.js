import {
  brute,
  big,
  step
} from './count'

import * as ELEMENTS from './elements'

import '../style/app.scss'

const THRESHOLD = 20000;

function displayResult(result) {
  ELEMENTS.RESULT_COUNT.innerHTML = `There are <strong>${result.count}</strong> 3s.`;
  if (Array.isArray(result.numbers)) {
    ELEMENTS.RESULT_NUMBERS.textContent = result.numbers.join(', ');
  } else {
    ELEMENTS.RESULT_NUMBERS.textContent = result.numbers;
  }
}

function clearResult() {
  ELEMENTS.RESULT_COUNT.textContent = '';
  ELEMENTS.RESULT_NUMBERS.textContent = '';
}

function displayErrors(errors) {
  ELEMENTS.ERRORS.style.display = 'block';
  ELEMENTS.ERRORS.innerHTML = `<ul>${errors.map(error => `<li>${error}</li>`).join('')}</ul>`;
}

function clearErrors() {
  ELEMENTS.ERRORS.style.display = 'none';
  ELEMENTS.ERRORS.innerHTML = '';
}

function filterInput(input) {
  // Remove everything that is not a number.
  return parseInt(input.replace(/[^\d]/g, ''));
}

const submit = () => {
  return new Promise((resolve, reject) => {
    // Get filtered values.
    let min = filterInput(ELEMENTS.INPUT_MIN.value);
    let max = filterInput(ELEMENTS.INPUT_MAX.value);
    // Validate input.
    let errors = [];
    if (typeof min !== 'number' || isNaN(min)) {
      errors.push('Invalid min value, please enter a number.');
    }
    if (typeof min !== 'number' || isNaN(max)) {
      errors.push('Invalid max value, please enter a number.');
    }
    if (min > max) {
      errors.push('Min cannot be bigger than max.');
    }
    // Reject if input is not valid.
    if (errors.length > 0) {
      reject(errors);
    }
    // Use different algorithm depending on the scale of numbers.
    if (max - min > THRESHOLD) {
      let result = big(max);
      result.count -= big(min).count;
      resolve(result);
    } else {
      step(min, max)
        .then(result => {
          resolve(result);
        });
    }
  });
}

const formHandler = (event) => {
  event.preventDefault();
  // Disable submit while form is being processed.
  ELEMENTS.SUBMIT.disabled = true;
  submit()
    .then(result => {
      ELEMENTS.SUBMIT.disabled = false;
      clearErrors();
      displayResult(result);
    })
    .catch(errors => {
      ELEMENTS.SUBMIT.disabled = false;
      clearResult();
      displayErrors(errors);
    })
}

ELEMENTS.FORM.addEventListener('submit', formHandler);

import './test';