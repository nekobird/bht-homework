const SUBJECT_NUMBER = 3;
const TIMEOUT = 5000;

// TODO: move constants to its own module.
// TODO: Clean up this stuff into a class?

// b looks at tenth power and above
// and finds the number within
// Using this pattern
// 10  - 1
// 100 - 20
// 1000 - 300
// and so on...
// d * (10^(p-1) * p)

// m looks at the case when the first
// digit matches the digit we want to count.
// 
// e are the digits to the right as a number
// b(d, p) + e(d, n, p) + 1

// above
// 59 -> b

function below(digit, power) {
  digit = parseInt(digit);
  return power === 1 ? digit : digit * (Math.pow(10, power - 1) * power);
}

function middle(digit, digits, power) {
  digits = [...digits.toString()];
  let index = digits.length - power;
  let excess = parseInt(digits.slice(index).join(''));
  return below(digit, power) + excess + 1;
}

function above(digit, power) {
  return below(digit, power) + Math.pow(10, power);
}

// Use lookup table.
export const big = max => {
  const startTime = Date.now();
  if (max === 0) return {
    count: 0,
    numbers: 'n/a',
    duration: 0
  }
  let digits = [...max.toString()];
  let count = 0;
  let power = digits.length - 1;
  digits.forEach(digit => {
    digit = parseInt(digit);
    if (power > 0) {
      if (digit < SUBJECT_NUMBER) {
        count += below(digit, power);
      } else if (digit === SUBJECT_NUMBER) {
        count += middle(digit, max, power);
      } else if (digit > SUBJECT_NUMBER) {
        count += above(digit, power);
      }
    } else {
      count += digit >= SUBJECT_NUMBER ? 1 : 0;
    }
    power--;
  });
  return {
    count,
    numbers: 'There are too many to display.',
    duration: Date.now() - startTime
  };
}

// Generate
export const step = (min, max) => {
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject(new Error('Exceeded timeout'));
    }, TIMEOUT);
    const startTime = Date.now();
    let count = 0;
    let numbers = [];
    let step = min;
    while (step <= max) {
      let digits = step.toString().split('');
      let matches = digits.filter(digit => parseInt(digit) === SUBJECT_NUMBER).length;
      if (matches > 0) {
        numbers.push(step);
        count += matches;
        step++;
      } else {
        // Jump!
        if (digits[digits.length - 1] == 0) {
          // If zero jump to num.
          step += SUBJECT_NUMBER;
        } else if (digits[digits.length - 1] < SUBJECT_NUMBER) {
          // If below jump to num.
          step += SUBJECT_NUMBER - digits[digits.length - 1];
        } else {
          // If above jump to tenth.
          step += Math.abs(10 - digits[digits.length - 1]);
        }
      }
    }
    clearTimeout(timeout);
    resolve({
      count,
      numbers,
      duration: Date.now() - startTime
    });
  });
}

// Brute force method to verify the result.
// noprotect
export const brute = (min, max) => {
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject(new Error('Exceeded timeout'))
    }, TIMEOUT)
    const startTime = Date.now()
    let count = 0;
    let numbers = [];
    for (let step = min; step <= max; step++) {
      let digits = step.toString().split('');
      let matches = digits.filter(digit => parseInt(digit) === SUBJECT_NUMBER).length;
      count += matches;
      numbers.push(step);
    }
    clearTimeout(timeout);
    resolve({
      count,
      numbers,
      duration: Date.now() - startTime
    });
  })

}